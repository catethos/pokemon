import pickle
from random import sample
import flask
from flask import Flask
from flask import request
from flask_cors import CORS, cross_origin
import json

with open('rules_pokemon_filtered_k2.pickle', 'rb') as handle:
    a = pickle.load(handle)
    
def find_pokemon(input_list):
    tmp = sorted(input_list)
    tmp2 = ",".join(tmp)
    df = a[a['LHS']==tmp2].sort_values(by=["Confidence"],ascending=False)
    r = df["RHS"].tolist()
    s = df["Confidence"].tolist()
    big= []
    for i in range(len(r)):
        if [r[i],s[i]] not in big:
            big.append([r[i],s[i]])
            
    if len(big) == 0:
        return find_pokemon(sample(input_list,len(input_list)-1))
    else:
        return big[:5]
    
    
app = Flask(__name__)
CORS(app)

@app.route("/echo",methods=["POST"])
def g():
    resp = request.data.decode("utf8")
    selected_pokemons = json.loads(resp)['selected']
    recommend_pokemons = find_pokemon(selected_pokemons)
    recommend_pokemons = [[x,"{0:.2f}".format(y)] for x,y in recommend_pokemons]
    return flask.jsonify(recommend_pokemons)

@app.route("/predict",methods=["POST"])
def f():
    inputs = ast.literal_eval(request.data.decode("utf8"))
    output_pokemons = find_pokemon(inputs["data"])
    return str(output_pokemons)
    
    
if __name__ == "__main__":        
    app.run(port="5555")